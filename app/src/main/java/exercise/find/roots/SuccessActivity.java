package exercise.find.roots;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
//* open a new "success" screen showing the following data:
//  - the original input number
//  - 2 roots combining this number (e.g. if the input was 99 then you can show "99=9*11" or "99=3*33"
//  - calculation time in seconds
public class SuccessActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);
        Intent last = getIntent();

        TextView origin = findViewById(R.id.original);
        TextView root1 = findViewById(R.id.root1);
        TextView root2 = findViewById(R.id.root2);
        TextView calcT= findViewById(R.id.calctime);
        String s1 = "Original num is : "+last.getLongExtra("origin",0);
        String s2 = "root 2 num is : "+last.getLongExtra("root2",0);
        String s3 = "root 1 num is : "+last.getLongExtra("root1",0);
        String s4 = "Calculation time is : "+ last.getDoubleExtra("calc_time",0);
        origin.setText(s1);
        root1.setText(s3);
        root2.setText(s2);
        calcT.setText(s4);

    }

}
