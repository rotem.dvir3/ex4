package exercise.find.roots;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class CalculateRootsService extends IntentService {


  public CalculateRootsService() {
    super("CalculateRootsService");
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    if (intent == null) return;
    long timeStartMs = System.currentTimeMillis();
    long numberToCalculateRootsFor = intent.getLongExtra("number_for_service", 0);
    if (numberToCalculateRootsFor <= 0) {
      Log.e("CalculateRootsService", "can't calculate roots for non-positive input" + numberToCalculateRootsFor);
      return;
    }
    long i = 2;
    long res =0 ;
    long cur_time = System.currentTimeMillis();
    while(cur_time-timeStartMs<=20000 && i<numberToCalculateRootsFor ){
      if (numberToCalculateRootsFor%i==0){
        res = numberToCalculateRootsFor/i;
        break;
      }
      i++;
      cur_time=System.currentTimeMillis();
    }
    float time = (float)(cur_time-timeStartMs)/(float)1000;

    if (cur_time-timeStartMs>20000){
      Intent new_int = new Intent("stopped_calculations");
      new_int.putExtra("time_until_give_up_seconds",time);
      this.sendBroadcast(new_int);
      return;

    }
    Intent new_int = new Intent("found_roots");
    new_int.putExtra("original_number",numberToCalculateRootsFor);
    if (res!=0 ){
      new_int.setAction("found_roots");

      new_int.putExtra("root1",i);
      new_int.putExtra("root2",res);
    }
    else if(i==numberToCalculateRootsFor){
      new_int.setAction("found_roots");

      new_int.putExtra("root1",1);
      new_int.putExtra("root2",numberToCalculateRootsFor);

    }
    new_int.putExtra("time_until_give_up_seconds",res);

    this.sendBroadcast(new_int);





  }


}