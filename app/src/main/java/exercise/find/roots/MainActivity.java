package exercise.find.roots;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.opengl.Visibility;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

  private BroadcastReceiver broadcastReceiverForSuccess = null;
  private BroadcastReceiver broadcastReceiverForFail = null;



  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ProgressBar progressBar = findViewById(R.id.progressBar);
    EditText editTextUserInput = findViewById(R.id.editTextInputNumber);
    Button buttonCalculateRoots = findViewById(R.id.buttonCalculateRoots);

    // set initial UI:
    progressBar.setVisibility(View.GONE); // hide progress
    editTextUserInput.setText(""); // cleanup text in edit-text
    editTextUserInput.setEnabled(true); // set edit-text as enabled (user can input text)
    buttonCalculateRoots.setEnabled(false); // set button as disabled (user can't click)

    // set listener on the input written by the keyboard to the edit-text
    editTextUserInput.addTextChangedListener(new TextWatcher() {
      public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
      public void onTextChanged(CharSequence s, int start, int before, int count) { }
      public void afterTextChanged(Editable s) {
        //the edit-text behavior is:
        //* when there is a calculation in the BG, edit-text is disabled (user can't input anything)
        //* otherwise (not calculating anything in the BG), edit-text is enabled (user can tap to open the keyboard and add input)
        String newText = editTextUserInput.getText().toString();
        long userInputLong = 0;
        try{
          userInputLong= Long.parseLong(newText);

        }catch (NumberFormatException e){
          userInputLong=-1;
        }
        buttonCalculateRoots.setEnabled(userInputLong>-1);



      }
    });

    // set click-listener to the button
    buttonCalculateRoots.setOnClickListener(v -> {
      Intent intentToOpenService = new Intent(MainActivity.this, CalculateRootsService.class);
      String userInputString = editTextUserInput.getText().toString();
      long userInputLong = Long.parseLong(userInputString);
      intentToOpenService.putExtra("number_for_service", userInputLong);
      startService(intentToOpenService);
      editTextUserInput.setEnabled(false);
      buttonCalculateRoots.setEnabled(false);
      progressBar.setVisibility(View.VISIBLE);

    });

    // register a broadcast-receiver to handle action "found_roots"
    broadcastReceiverForSuccess = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent incomingIntent) {

        if (incomingIntent == null || !incomingIntent.getAction().equals("found_roots")) return;
        editTextUserInput.setEnabled(true);
        buttonCalculateRoots.setEnabled(true);
        progressBar.setVisibility(View.INVISIBLE);
        long origin = incomingIntent.getLongExtra("original_number",-1);
        long root2 = incomingIntent.getLongExtra("root2",-1);
        long root1 = incomingIntent.getLongExtra("root1",-1);
        long time1 = incomingIntent.getLongExtra("time_until_give_up_seconds",-1);
        Intent succ_int =  new Intent(MainActivity.this, SuccessActivity.class);
        succ_int.putExtra("origin",origin);
        succ_int.putExtra("root1",root1);
        succ_int.putExtra("root2",root2);
        succ_int.putExtra("calc_time",time1);
        startActivity(succ_int);

      }
    };
    registerReceiver(broadcastReceiverForSuccess, new IntentFilter("found_roots"));

    broadcastReceiverForFail = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent incomingIntent) {
        if (incomingIntent == null || !incomingIntent.getAction().equals("stopped_calculations")) return;

        editTextUserInput.setEnabled(true);
        buttonCalculateRoots.setEnabled(true);
        progressBar.setVisibility(View.INVISIBLE);
        String s = "calculation aborted after"+
                incomingIntent.getLongExtra("time_until_give_up_seconds",0)+"seconds";
        Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG).show();

      }
    };
    registerReceiver(broadcastReceiverForFail, new IntentFilter("stopped_calculations"));





  }


  @Override
  protected void onDestroy() {
    super.onDestroy();
    this.unregisterReceiver(broadcastReceiverForFail);
    this.unregisterReceiver(broadcastReceiverForSuccess);

  }

  @Override
  protected void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    EditText editTextUserInput = findViewById(R.id.editTextInputNumber);
    String s =editTextUserInput.getText().toString();
    ProgressBar progressBar = findViewById(R.id.progressBar);
    Button buttonCalculateRoots = findViewById(R.id.buttonCalculateRoots);

    outState.putInt("progrees_shown",progressBar.getVisibility());
    outState.putBoolean("button_enabled",buttonCalculateRoots.isEnabled());
    outState.putBoolean("text_enabled",editTextUserInput.isEnabled());

    outState.putString("user_last_input",s);

  }

  @Override
  protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    EditText editTextUserInput = findViewById(R.id.editTextInputNumber);
    ProgressBar progressBar = findViewById(R.id.progressBar);
    Button buttonCalculateRoots = findViewById(R.id.buttonCalculateRoots);
    editTextUserInput.setText(savedInstanceState.getString("user_last_input"));
    progressBar.setVisibility(savedInstanceState.getInt("progrees_shown"));
    buttonCalculateRoots.setEnabled(savedInstanceState.getBoolean("button_enabled"));
    editTextUserInput.setEnabled(savedInstanceState.getBoolean("text_enabled"));


  }
}


/*

the spec is:

upon launch, Activity starts out "clean":
* progress-bar is hidden
* "input" edit-text has no input and it is enabled
* "calculate roots" button is disabled

the button behavior is:
* when there is no valid-number as an input in the edit-text, button is disabled
* when we triggered a calculation and still didn't get any result, button is disabled
* otherwise (valid number && not calculating anything in the BG), button is enabled

the edit-text behavior is:
* when there is a calculation in the BG, edit-text is disabled (user can't input anything)
* otherwise (not calculating anything in the BG), edit-text is enabled (user can tap to open the keyboard and add input)

the progress behavior is:
* when there is a calculation in the BG, progress is showing
* otherwise (not calculating anything in the BG), progress is hidden

when "calculate roots" button is clicked:
* change states for the progress, edit-text and button as needed, so user can't interact with the screen

when calculation is complete successfully:
* change states for the progress, edit-text and button as needed, so the screen can accept new input
* open a new "success" screen showing the following data:
  - the original input number
  - 2 roots combining this number (e.g. if the input was 99 then you can show "99=9*11" or "99=3*33"
  - calculation time in seconds

when calculation is aborted as it took too much time:
* change states for the progress, edit-text and button as needed, so the screen can accept new input
* show a toast "calculation aborted after X seconds"


upon screen rotation (saveState && loadState) the new screen should show exactly the same state as the old screen. this means:
* edit-text shows the same input
* edit-text is disabled/enabled based on current "is waiting for calculation?" state
* progress is showing/hidden based on current "is waiting for calculation?" state
* button is enabled/disabled based on current "is waiting for calculation?" state && there is a valid number in the edit-text input


 */